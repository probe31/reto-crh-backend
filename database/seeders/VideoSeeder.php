<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class VideoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('videos')->insert([
            'title' => '1. Introducción',
            'url' => 'https://crehana-videos.akamaized.net/outputs/trailer/89ef7d652e4549709347f89aa7be0f57/1f68b3fffd1641c0b03d1457a53808d4.m3u8',
            'course_id' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('videos')->insert([
            'title' => '2. Typescripy y NodeJS',
            'url' => 'https://crehana-videos.akamaized.net/outputs/trailer/29f6759c6bd04a5f9cdd4e351fc3ddde/d0da0c55b987479fafd7fb68ca298e5f.m3u8',
            'course_id' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('videos')->insert([
            'title' => '3. RxJS: Observables',
            'url' => 'https://crehana-videos.akamaized.net/outputs/trailer/d6fa2199a76c4a62bad13d2431797061/34d25cc19c4b4e9f9a967fc9c2ed2902.m3u8',
            'course_id' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('videos')->insert([
            'title' => '4. Primer proyecto: análisis de estructura',
            'url' => 'https://crehana-videos.akamaized.net/outputs/trailer/e0a7d08a579240a2bcf4d9051e625977/7ae20874869443c2be01f672424e85b0.m3u8',
            'course_id' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('videos')->insert([
            'title' => '5. Directivas',
            'url' => 'https://crehana-videos.akamaized.net/outputs/trailer/1936e256200f43068f7bbc0aaba236c9/e188e88593d54c55be8150426eed4158.m3u8',
            'course_id' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }
}
